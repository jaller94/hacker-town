# Protocol Specification

This protocol describes how a client can request a video game session on a server via Matrix.

It uses the following terminology:
* Player – A human individual who wants to play the game. By default, there's a 1-to-1-relationship of one Player and one Matrix account, if not noted otherwise.
* Client – A software run by a player to join a game. This software can connect to a Matrix server to negotiate a session and to a game server to play the game.
* Character – A virtual avatar which represents a player in a game and can interact with the game world. A character may be owned and used by multiple players, however, for most games only one client can use a character at any point in time if its progression shall be stored.
* Game server – A central server to play the game on. It retrieves actions from clients, sends the game state to clients and enforces the rules of the game, aka protects against cheating. The game server decides which character data it accepts and stores the character data at the end of a session.
* Session – An active connection between a client and a game server. It's associated with a character and for most games locks the character to not be used for another session at the same time. A session may be spread over multiple HTTP requests in which case a timeout determines when a session becomes inactive and is therefore terminated.

Games are played via an HTTP(S), Websockets or WebRTC. Games are not played by sending events in a Matrix room as this would be too slow and wasteful. Most game state like a player moving around is temporary and shouldn't be stored in a Matrix room.

Matrix is used for two main purposes:
* Moderation of who can play on a server
* Giving players ownership over their character data

These two main purposes are represented in Lobby rooms and Character rooms respectively.

## Lobby rooms

A lobby room is used to manage who is allowed to play on a specific server. One lobby room MUST point to only one game server. Many lobby rooms MAY point to the same game server. A server can either kick Matrix accounts after they have ended their session to or allow them to stay in the room.

Every lobby room MUST have `"type": "de.chrpaul.city.lobby"` in its `m.room.create` state event. This event can only be set when a Matrix room is created.

Using the a MatrixClient of the NodeJs package `matrix-bot-sdk`, a public lobby room can be created like this:

```js
await client.createRoom({
    creation_content: {
        type: 'de.chrpaul.city.lobby',
    },
    name: 'CityGame by Jaller94',
    preset: 'public_chat',
});
```

### Going online

To signal to clients that a game server is online, it SHOULD post a state event of the type `de.chrpaul.city.lobby` in a lobby room. This state MUST NOT have a key.

```json
{
    "apiUrl": "https://city.chrpaul.de/api",
    "matrixId": "@citygame:matrix.org"
}
```

The attribute `apiUrl` tells clients the base URL for the server's HTTP(S) API. The attribute `matrixId` is used by client's to know which Matrix account belongs to the server. Clients MUST only process response events coming from this Matrix ID.

### Going offline

To singal that a server is going offline, it SHOULD post an empty content to the previously described state event `de.chrpaul.city.lobby` in a lobby room.

```js
await this.client.sendStateEvent(lobbyRoomId, 'de.chrpaul.city.lobby', '', {});
```

### Client requests a character

#### Character request

A client may request a new character by posting a `de.chrpaul.city.character-request` event.

```json
{
    "name": "Tay the Demonslayer"
}
```

#### Character response

The server should reply to this request by posting a `de.chrpaul.city.character-response` event. This may contain an error description or a success message.

This is a sensible response, if the client did not specify a character name:

```json
{
    "errcode": "E_BAD_REQUEST"
}
```

If the character creation was successful, the game server MUST invite the client's Matrix account to the character room. In addition, the following reply should be send.

```json
{
    "characterRoomId": "!zJiEtqdVplmFhqFQxK:matrix.org"
}
```

## Character rooms

A character room allows players to own a character and its progression data. Based on the type of game, a game server should post a character's inventory, money balance, experience points, achievements and quest states to this room.

A game server may allow the player to invite other players to a character room. In this case the players can share one character or one player can migrate their character to a new Matrix account.

### Client requests a session

To play on the game server, a client needs to request a session. If a session got created, the client can authenticate to the HTTP(S) API using the session's access token.

#### Session request

A client may request a session by posting a `de.chrpaul.city.session-request` event. A session is commonly associated with a lobby and a character.

```json
{
    "characterRoomId": "!zJiEtqdVplmFhqFQxK:matrix.org",
    "clientId": "abcdefghi",
    "lobbyRoomId": "!OobRrsCsALXkplGpnJ:matrix.org"
}
```

It is in the client's interest to generate an ID for itself. As other clients of a player may read the game server's response, the client ID allows to determine if an access token is meant for the current client. This client ID may be a one-time ID and a client may pick a different ID for different requests.

#### Session response

The server should reply to this request by posting a `de.chrpaul.city.session-response` event. This may contain an error description or a success message.

This is a sensible response, if the client did not specify a character name:

```json
{
    "errcode": "E_CHARACTER_LOCKED"
}
```

If the character creation was successful, the game server MAY post a lock state event in the character room. In addition, the following reply MUST be send.

```json
{
    "accessToken": "abcdefghijk123456789"
}
```

## HTTP(S) API

All URL paths in this section are relative to the `apiUrl` value in a `de.chrpaul.city.lobby` state event. That means the absolute URL of `GET /end-session` may be `https://city.chrpaul.de/api/end-session`.

Most requests require a valid session. Clients must send an access token via the HTTP Request Header `Authorization` with the prefix "Bearer ". Here is an example:

```
Authorization: Bearer abcdefghijk123456789
```

### POST /end-session (required)

Allows clients to terminate a session instead of waiting for it to time out.

Pseudocode:
```
if (accessToken is valid) {
    1. Invalidate access token
    2. Store character data
}
```

### POST /guest-session (optional)

A game server may be configured to allow guest sessions. These session have no persistent character associated and may not require the client to have a Matrix account in a lobby room.

Allowing guest sessions is great for demo servers or where moderation via Matrix lobby rooms isn't needed.

An HTTP 200 response should look like this:

```json
{
    "accessToken": "abcdefghijk123456789"
}
```
