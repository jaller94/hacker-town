# City Game Specification

This specification is specific to the City game. It builds upon the general specification.

# Schemas

## `de.chrpaul.city.character` state event

TODO: Pick an `$id` for this JSON Schema.

```json
{
    "$schema": "http://json-schema.org/draft-07/schema",
    "$id": "http://example.com/example.json",
    "type": "object",
    "title": "state event: de.chrpaul.city.character",
    "description": "A character's progress in-game progress.",
    "default": {},
    "examples": [
        {
            "money": 0,
            "x": 6,
            "y": 2
        }
    ],
    "required": [
        "x",
        "y"
    ],
    "properties": {
        "money": {
            "$id": "#/properties/money",
            "default": 0,
            "description": "The amount of money this character has.",
            "examples": [
                342
            ],
            "minimum": 0,
            "title": "Amount of money",
            "type": "integer"
        },
        "x": {
            "$id": "#/properties/x",
            "description": "The x coordinate on the map at which the player should spawn back in.",
            "examples": [
                0
            ],
            "title": "x position",
            "type": "integer"
        },
        "y": {
            "$id": "#/properties/y",
            "description": "The y coordinate on the map at which the player should spawn back in.",
            "examples": [
                0
            ],
            "title": "y position",
            "type": "integer"
        }
    },
    "additionalProperties": true
}
```
