# Schemas

TODO: Pick an `$id`s for the JSON Schemas.

## `de.chrpaul.city.lobby` state event

```json
{
    "$schema": "https://json-schema.org/draft/2020-12/schema",
    "$id": "https://example.com/example.json",
    "type": "object",
    "title": "state event: de.chrpaul.city.lobby",
    "description": "Game servers should post these state events in lobby rooms.",
    "examples": [
        {
            "apiUrl": "https://city.chrpaul.de/api",
            "matrixId": "@citygame:matrix.org"
        }
    ],
    "required": [
        "apiUrl",
        "matrixId"
    ],
    "properties": {
        "apiUrl": {
            "$id": "#/properties/apiUrl",
            "description": "The API URL of a game server is an absolute URL where clients connect to in oder to start a game session.",
            "examples": [
                "https://city.chrpaul.de/api",
                "http://localhost:3000"
            ],
            "title": "Url of the HTTP(S) API of the game server",
            "type": "string"
        },
        "matrixId": {
            "$id": "#/properties/matrixId",
            "description": "The Matrix ID which the game server uses to respond. Clients expect character and session requests to be answered by this Matrix ID.",
            "examples": [
                "@citygame:matrix.org"
            ],
            "title": "Matrix ID of the game server",
            "type": "string"
        }
    },
    "additionalProperties": true
}
```

## `de.chrpaul.city.character.lock` state event

```json
{
    "$schema": "https://json-schema.org/draft/2020-12/schema",
    "$id": "https://example.com/example.json",
    "type": "object",
    "title": "state event: de.chrpaul.city.character.lock",
    "description": "Game servers should post these in character rooms to indicate when a character is in use. No new sessions may be issued for other clients or on other game servers until the lock expires or is overwritten.",
    "examples": [
        {
            "clientAccessToken": "city_ead1b49c24736cac6cd4297018efa9f4",
            "clientId": "abc",
            "serverId": "test",
            "untilTs": 1635522523571
        }
    ],
    "required": [
        "serverId",
        "untilTs"
    ],
    "properties": {
        "clientAccessToken": {
            "$id": "#/properties/clientAccessToken",
            "title": "Client access token",
            "type": "string"
        },
        "clientId": {
            "$id": "#/properties/matrixId",
            "description": "A schemaless string which the client provided to avoid collisions with other clients.",
            "title": "Client ID",
            "type": "string"
        },
        "serverId": {
            "$id": "#/properties/matrixId",
            "description": "A schemaless string which the game server generated to avoid collisions with other game servers.",
            "title": "Server ID",
            "type": "string"
        },
        "untilTs": {
            "$id": "#/properties/untilTs",
            "description": "A unix time stamp in milliseconds until which the lock is valid, if it is not renewed or removed.",
            "title": "Until Timestamp",
            "type": "integer"
        }
    },
    "additionalProperties": true
}
```

## `de.chrpaul.city.character-request` event

```json
{
    "$schema": "https://json-schema.org/draft/2020-12/schema",
    "$id": "https://example.com/example.json",
    "type": "object",
    "title": "event: de.chrpaul.city.character-request",
    "description": "Clients post character requests to create a Matrix room a persistent storage of their game progress.",
    "examples": [
        {
            "name": "Tay the Demonslayer"
        }
    ],
    "required": [
        "name"
    ],
    "properties": {
        "name": {
            "$id": "#/properties/name",
            "description": "The name of the character room. This may be used as the in-game display name for this character.",
            "examples": [
                "Tay the Demonslayer",
            ],
            "title": "Character name",
            "type": "string"
        },
    },
    "additionalProperties": true
}
```

## `de.chrpaul.city.character-response` event

```json
{
    "$schema": "https://json-schema.org/draft/2020-12/schema",
    "$id": "https://example.com/example.json",
    "type": "object",
    "title": "event: de.chrpaul.city.character-response",
    "description": "Game servers post these to respond to a client's character request.",
    "examples": [
        {
            "characterRoomId": "!zJiEtqdVplmFhqFQxK:matrix.org"
        }
    ],
    "required": [],
    "properties": {
        "characterRoomId": {
            "$id": "#/properties/characterRoomId",
            "description": "The Matrix room ID of the created character.",
            "examples": [
                "!zJiEtqdVplmFhqFQxK:matrix.org",
            ],
            "title": "Character room ID",
            "type": "string"
        },
        "errcode": {
            "$id": "#/properties/errcode",
            "description": "An error code, giving the client a hint as to why no session could be created. Use E_UNKNOWN for unexpected server-side errors.",
            "examples": [
                "E_BAD_REQUEST",
                "E_UNKNOWN",
            ],
            "title": "Error code",
            "type": "string"
        },
    },
    "additionalProperties": true
}
```

## `de.chrpaul.city.session-request` event

```json
{
    "$schema": "https://json-schema.org/draft/2020-12/schema",
    "$id": "https://example.com/example.json",
    "type": "object",
    "title": "event: de.chrpaul.city.session-request",
    "description": "Clients post session request to receive an access token and allow their player to play the game.",
    "examples": [
        {
            "characterRoomId": "!zJiEtqdVplmFhqFQxK:matrix.org",
            "clientId": "abcdefghi",
            "lobbyRoomId": "!OobRrsCsALXkplGpnJ:matrix.org"
        }
    ],
    "required": [
        "lobbyRoomId"
    ],
    "properties": {
        "characterRoomId": {
            "$id": "#/properties/characterRoomId",
            "description": "The Matrix room ID of the character to play as.",
            "examples": [
                "!zJiEtqdVplmFhqFQxK:matrix.org",
            ],
            "title": "Character Room ID",
            "type": "string"
        },
        "clientId": {
            "$id": "#/properties/clientId",
            "description": "A schemaless ID which the client can use to determine if a session response and character lock are meant for itself.",
            "examples": [
                "abcdefghi",
            ],
            "title": "Client ID",
            "type": "string"
        },
        "lobbyRoomId": {
            "$id": "#/properties/lobbyRoomId",
            "description": "The Matrix room ID of the lobby to play in.",
            "examples": [
                "!OobRrsCsALXkplGpnJ:matrix.org"
            ],
            "title": "Matrix ID of the game server",
            "type": "string"
        }
    },
    "additionalProperties": true
}
```

## `de.chrpaul.city.session-response` event

```json
{
    "$schema": "https://json-schema.org/draft/2020-12/schema",
    "$id": "https://example.com/example.json",
    "type": "object",
    "title": "event: de.chrpaul.city.session-response",
    "description": "Game servers post these to respond to a client's session request.",
    "examples": [
        {
            "accessToken": "abcdefghijk123456789"
        }
    ],
    "required": [],
    "properties": {
        "accessToken": {
            "$id": "#/properties/accessToken",
            "description": "An access token which the client can use to authenticate itself towards the game server's HTTP(S) API.",
            "examples": [
                "abcdefghijk123456789",
            ],
            "title": "Access token",
            "type": "string"
        },
        "errcode": {
            "$id": "#/properties/errcode",
            "description": "An error code, giving the client a hint as to why no session could be created. Use E_UNKNOWN for unexpected server-side errors.",
            "examples": [
                "E_BAD_REQUEST",
                "E_CHARACTER_LOCKED",
                "E_UNKNOWN",
            ],
            "title": "Error code",
            "type": "string"
        },
    },
    "additionalProperties": true
}
```
