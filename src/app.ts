import path from 'path';
import express from 'express';
import api from './api';
import log from './logger';
import { GameMap } from './index';
import MatrixController from './matrix';
import SessionsController, { Session } from './sessions';

declare global {
    namespace Express {
        interface Request {
            gameSession: Session,
        }
    }
}

const App = (map: GameMap, matrixController: MatrixController, sessionsController: SessionsController) => {
    matrixController.start(sessionsController).catch(async(error: unknown) => {
        console.error('Failed to start MatrixController');
        console.error(error);
        await matrixController.stop();
        process.exit(1);
    });
    
    process.on('SIGTERM', async() => {
        console.log('Closing sessions.');
        await sessionsController.stop();
        await matrixController.stop();
        console.log('Done. Have a nice day.');
    })
    
    const app = express();
    // Security:
    // https://expressjs.com/en/advanced/best-practice-security.html#at-a-minimum-disable-x-powered-by-header
    app.disable('x-powered-by');

    app.all('*', (req, res, next) => {
        log.info('incoming request', { method: req.method, url: req.url });
        next();
    });

    const corsWildcard = (req: express.Request, res: express.Response, next: any) => {
        res.header('Access-Control-Allow-Origin', '*');
        res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
        next();
    };


    app.use('/api', corsWildcard, api(map, sessionsController));

    app.use(express.static(path.join(__dirname, 'public')));

    app.use('/htm/preact', express.static(path.join(__dirname, '../node_modules/htm/preact')));
    app.use('/matrix-js-sdk', express.static(path.join(__dirname, '../node_modules/matrix-js-sdk/dist')));

    return app;
}

export default App;
