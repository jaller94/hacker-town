import MatrixController from './matrix';
import { Player } from './api';

export type Session = {
    matrix?: {
        characterId: string;
        clientId: string;
        lockValidUntilTs: number;
        lockValidTimeout: NodeJS.Timeout;
    };
    liveData: Player;
    godMode: boolean;
    storedData: Record<string, unknown>|undefined;
};

const MS_TO_SAVE = 60 * 1000;

const generateAccessToken = () => {
    const chars = '0123456789abcedf'.split('');
    let token = '';
    for (let i = 0; i < 32; i++) {
        token += chars[Math.floor(Math.random() * chars.length)];
    }
    return `city_${token}`;
};


const getRandomItem = <T>(arr: T[]): T => {
    return arr[Math.floor(arr.length * Math.random())];
};

// const generateUsername = (): string => {
//     const animals = 'Ameise,Bär,Biene,Corgi,Delfin,Eule,Fisch,Fuchs,Gans,Giraffe,Hase,Hund,Katze,Löwe,Meerschweinchen,Tiger,Wal'.split(',');
//     return `${getRandomItem(animals)}${getRandomItem('123456789'.split(''))}${getRandomItem('12345679'.split(''))}`;
// };

const generateUsername = (): string => {
    const adjectives = 'angry,best,bitter,chill,evil,fast,funny,funky,good,groovy,helpful,lazy,magnificent,sleepy,small,tall'.split(',');
    const animals = 'alpaca,ant,bear,bobcat,camel,cat,corgi,dog,deer,dolphin,elephant,fox,giraffe,goose,lama,lion,otter,owl,tiger,whale'.split(',');
    return `${getRandomItem(adjectives)} ${getRandomItem(animals)}`;
};

// TODO Renew locks before they expire
// TODO Expire accessTokens after untilTs

export default class SessionsController {
    private sessions: Map<string, Session> = new Map();

    constructor(private matrixController: MatrixController) {
    }

    public addSession(storedData?: Record<string, unknown>, matrix?: {characterId: string, clientId: string, untilTs: number, displayName?: string}): string {
        const token = generateAccessToken();
        if (this.sessions.has(token)) {
            throw Error('Failed to generate unique token.');
        }
        const liveData = {
            displayName: matrix?.displayName || generateUsername(),
            money: 0,
            // x: 1,
            // y: 0,
            // Spawn location
            x: 0,
            y: 0,
            lastActivity: Date.now(),
        };
        if (storedData) {
            if (typeof storedData.x === 'number' && typeof storedData.y === 'number') {
                liveData.x = storedData.x;
                liveData.y = storedData.y;
            }
            if (typeof storedData.money === 'number') {
                liveData.money = storedData.money;
            }
        }
        const session: Session = {
            liveData,
            storedData,
            godMode: true,
        };
        if (matrix) {
            session.matrix = {
                characterId: matrix.characterId,
                clientId: matrix.clientId,
                lockValidUntilTs: matrix.untilTs,
                lockValidTimeout: setTimeout(async() => {
                    if (this.shouldRenewLock(token)) {
                        await this.renewLock(token);
                    } else {
                        await this.closeSession(token);
                    }
                }, matrix.untilTs - Date.now() - MS_TO_SAVE),
            };
        }
        this.sessions.set(token, session);
        return token;
    }

    public async closeSession(accessToken: string) {
        const session = this.getSession(accessToken);
        if (!session) return;
        this.removeSession(accessToken);
        if (session.matrix) {
            clearTimeout(session.matrix.lockValidTimeout);
            await this.matrixController.saveCharacter(session.matrix.characterId, {
                x: session.liveData.x,
                y: session.liveData.y,
            });
            try {
                await this.matrixController.unlockCharacter(session.matrix.characterId);
            } catch (error: unknown) {
                console.warn(`Failed to unlock character ${session.matrix.characterId}`, error);
            }
        }
    }

    public getSession(accessToken: string): Session|undefined {
        return this.sessions.get(accessToken);
    }

    public getSessionByCharacter(characterId: string): Session|undefined {
        for (const session of this.sessions.values()) {
            if (session.matrix && session.matrix.characterId === characterId) {
                return session;
            }
        }
    }

    public removeSession(accessToken: string): void {
        this.sessions.delete(accessToken);
    }

    public async renewLock(accessToken: string) {
        const session = this.getSession(accessToken);
        if (!session || !session.matrix) return;
        const untilTs = Date.now() + (10 * 60 * 1000);
        await this.matrixController.lockCharacter(session.matrix.characterId, session.matrix.clientId, accessToken, untilTs);
        session.matrix.lockValidUntilTs = untilTs;
        session.matrix.lockValidTimeout = setTimeout(async () => {
            if (this.shouldRenewLock(accessToken)) {
                await this.renewLock(accessToken);
            } else {
                await this.closeSession(accessToken);
            }
        }, untilTs - Date.now() - MS_TO_SAVE);
    }

    public shouldRenewLock(accessToken: string): boolean {
        const session = this.getSession(accessToken);
        if (session) {
            if (Date.now() - session.liveData.lastActivity < 2 * 60 * 1000) {
                return true;
            }
        }
        return false;
    }

    public async stop() {
        for (const token of this.sessions.keys()) {
            await this.closeSession(token);
        }
    }

    public get players(): Record<string, unknown>[] {
        return [...this.sessions.values()].map(session => session.liveData);
    }

    public get size(): number {
        return this.sessions.size;
    }
}
