import {
    MatrixClient,
    SimpleFsStorageProvider,
} from 'matrix-bot-sdk';
import SessionsController from './sessions';

enum RoomType {
    Character = 'de.chrpaul.city.character',
    Lobby = 'de.chrpaul.city.lobby',
}

enum EventType {
    CharacterResponse = 'de.chrpaul.city.character-response',
    CharacterRequest = 'de.chrpaul.city.character-request',
    SessionResponse = 'de.chrpaul.city.session-response',
    SessionRequest = 'de.chrpaul.city.session-request',
}

enum StateEventType {
    Character = 'de.chrpaul.city.character',
    CharacterLock = 'de.chrpaul.city.character.lock',
    LobbyServer = 'de.chrpaul.city.lobby.server',
}

type ResponseFunction = (content: string|Record<string, unknown>) => Promise<void>;

export default class MatrixController {
    private client: MatrixClient;
    private serverId: string;
    private sessionsController?: SessionsController;

    constructor(baseUrl: string, accessToken: string, private lobbyRoomId: string) {
        const storage = new SimpleFsStorageProvider('./webstorage/matrix.json');
        this.client = new MatrixClient(
            baseUrl,
            accessToken,
            storage,
        );
        // TODO Set a unique, but persistent serverId
        this.serverId = 'test';
    }

    public static async createLobbyRoom(baseUrl: string, accessToken: string, lobbyName: string): Promise<string> {
        const client = new MatrixClient(
            baseUrl,
            accessToken,
        );
        const lobbyRoomId = await client.createRoom({
            creation_content: {
                type: RoomType.Lobby,
            },
            name: lobbyName,
            preset: 'public_chat',
        });
        console.info(`Created lobby room ${lobbyRoomId}`);
        return lobbyRoomId;
    }

    /**
     * 1. Announce server address in the lobby room.
     * 2. Set up event handlers for Matrix events.
     */
    public async start(sessionsController: SessionsController) {
        this.sessionsController = sessionsController;
        // Announce server address in the lobby room.
        await this.client.sendStateEvent(this.lobbyRoomId, StateEventType.LobbyServer, '', {
            url: `${process.env.BASE_URL}/api`,
            userId: await this.client.getUserId(),
        });
        // Set up event handlers for Matrix events.
        await this.client.start();
        this.client.on('room.event', async (roomId, event) => {
            // Don't handle events that don't have contents (they were probably redacted)
            if (!event['content']) return;
            if (event['sender'] === await this.client.getUserId()) return;
            const respondFactory = (eventType: EventType) => async(content: string|Record<string, unknown>) => {
                if (typeof content === 'string') {
                    content = { errcode: content };
                    console.debug(`Responding with error ${content.errcode}`);
                }
                await this.client.sendEvent(roomId, eventType, content);
            };
            if (event['type'] === EventType.CharacterRequest) {
                const respond = respondFactory(EventType.CharacterResponse);
                if (typeof event['content']['name'] !== 'string') {
                    console.info('CharacterRequest without valid name');
                    return respond('E_BAD_REQUEST');
                }
                this.handleCharacterRequest({
                    matrixUser: event['sender'],
                    name: event['content']['name'],
                }, respond).catch(() => respond('E_UNKNOWN'));
            }
            if (event['type'] === EventType.SessionRequest) {
                const respond = respondFactory(EventType.SessionResponse);
                if (typeof event['content']['characterRoomId'] !== 'string') {
                    console.info('SessionRequest without valid characterRoomId');
                    return respond('E_BAD_REQUEST');
                }
                if (typeof event['content']['clientId'] !== 'string') {
                    console.info('SessionRequest without valid clientId');
                    return respond('E_BAD_REQUEST');
                }
                if (typeof event['content']['lobbyRoomId'] !== 'string') {
                    console.info('SessionRequest without valid lobbyRoomId');
                    return respond('E_BAD_REQUEST');
                }
                this.handleSessionRequest({
                    characterRoomId: event['content']['characterRoomId'],
                    clientId: event['content']['clientId'],
                    lobbyRoomId: event['content']['lobbyRoomId'],
                    matrixUser: event['sender'],
                }, respond).catch(() => respond('E_UNKNOWN'));
            }
        });
    }

    /**
     * 1. Announce server being offline in the lobby room.
     * 2. Stop listening to Matrix events.
     */
    public async stop() {
        if (this.lobbyRoomId) {
            await this.client.sendStateEvent(this.lobbyRoomId, StateEventType.LobbyServer, '', {});
        }
        this.client.stop();
    }

    /**
     * Called when a user requested to create a new character.
     * A user can own 0 to n characters.
     */
    public async handleCharacterRequest(args: {
        matrixUser: string,
        name: string,
    }, respond: ResponseFunction) {
        let characterRoomId;
        try {
            characterRoomId = await this.createCharacter(args.matrixUser, args.name);
        } catch {
            await respond('E_UNKNOWN');
        }
        await respond({
            characterRoomId,
        });
    }

    /**
     * Called when a user wants to play with a character in a lobby.
     * Sends the 
     * 1. Abort if character is locked.
     * 2. Issue an access token for the API.
     * 3. Lock the character.
     */
    public async handleSessionRequest(args: {
        characterRoomId: string,
        clientId: string,
        lobbyRoomId: string,
        matrixUser: string,
    }, respond: ResponseFunction) {
        if (!this.verifyLobbyMembership(args.lobbyRoomId, args.matrixUser)) {
            return await respond('E_NO_LOBBY_MEMBER');
        }
        if (!this.verifyCharacterOwnership(args.characterRoomId, args.matrixUser)) {
            return await respond('E_NO_CHARACTER_OWNER');
        }
        const storedData = await this.loadCharacter(args.characterRoomId);
        const lock = await this.loadCharacterLock(args.characterRoomId);
        if (lock && lock.serverId) {
            if (lock.serverId !== this.serverId || lock.clientId !== args.clientId) {
                return await respond('E_CHARACTER_LOCKED');
            }
            await respond({
                accessToken: lock.clientAccessToken,
            });
            return lock.clientAccessToken;
        } 
        const untilTs = Date.now() + (10 * 60 * 1000);
        if (!this.sessionsController) {
            throw ('sessionsController should have been defined.');
        }
        const characterName = await this.loadCharacterName(args.characterRoomId);
        const clientAccessToken = this.sessionsController.addSession(storedData, {
            characterId: args.characterRoomId,
            clientId: args.clientId,
            untilTs,
            displayName: characterName,
        });
        await this.lockCharacter(args.characterRoomId, args.clientId, clientAccessToken, untilTs);
        console.debug('Session started.', clientAccessToken)
        await respond({
            accessToken: clientAccessToken,
        });
        return clientAccessToken;
    }

    /**
     * 1. Create a character room.
     * 2. Invite user and and set their permission level.
     */
    public async createCharacter(matrixUser: string, name: string) {
        // Create a character room.
        const characterRoom = await this.client.createRoom({
            creation_content: {
                type: RoomType.Character,
            },
            power_level_content_override: {
                events: {
                    "m.room.name": 0, // Editable by the user; Character name
                    "m.room.power_levels": 100,
                    "m.room.topic": 100, // Used for human-readable progress description
                    [StateEventType.Character]: 100,
                    [StateEventType.CharacterLock]: 100,
                },
                redact: 100,
            },
            name,
            preset: 'private_chat',
        });
        // Invite user and and set their permission level.
        await this.client.inviteUser(matrixUser, characterRoom);
        await this.client.setUserPowerLevel(matrixUser, characterRoom, 50);
        return characterRoom;
    }

    public async loadCharacter(characterRoomId: string): Promise<Record<string, unknown>|undefined> {
        try {
            return await this.client.getRoomStateEvent(characterRoomId, StateEventType.Character, '');
        } catch (error: any) {
            if (error?.body?.errcode === 'M_NOT_FOUND') {
                return;
            }
            throw error;
        }
    }

    public async loadCharacterName(characterRoomId: string): Promise<string|undefined> {
        try {
            const content = await this.client.getRoomStateEvent(characterRoomId, 'm.room.name', '') as Record<string, unknown>;
            if (content && typeof content.name === 'string') {
                return content.name;
            }
        } catch (error: any) {
            if (error?.body?.errcode === 'M_NOT_FOUND') {
                return;
            }
            throw error;
        }
    }

    public async loadCharacterLock(characterRoomId: string): Promise<Record<string, unknown>|undefined> {
        try {
            const event = await this.client.getRoomStateEvent(characterRoomId, StateEventType.CharacterLock, '');
            if (event && typeof event.untilTs === 'number' && event.untilTs < Date.now()) {
                return;
            }
            return event;
        } catch (error: any) {
            if (error?.body?.errcode === 'M_NOT_FOUND') {
                return;
            }
            throw error;
        }
    }
    
    public async lockCharacter(characterRoomId: string, clientId: string, clientAccessToken: string, untilTs?: number) {
        await this.forceLockCharacter(characterRoomId, clientId, clientAccessToken, untilTs);
    }

    public async forceLockCharacter(characterRoomId: string, clientId: string, clientAccessToken: string, untilTs?: number) {
        await this.client.sendStateEvent(characterRoomId, StateEventType.CharacterLock, '', {
            serverId: this.serverId,
            clientAccessToken,
            clientId,
            untilTs, 
        });
    }
    
    public async unlockCharacter(characterRoomId: string): Promise<void> {
        await this.forceUnlockCharacter(characterRoomId);
    }

    public async forceUnlockCharacter(characterRoomId: string): Promise<void> {
        await this.client.sendStateEvent(characterRoomId, StateEventType.CharacterLock, '', {});
    }
    
    public async saveCharacter(characterRoomId: string, data: Record<string, unknown>): Promise<void> {
        const lock = await this.loadCharacterLock(characterRoomId);
        if (!lock || lock.serverId !== this.serverId) {
            throw Error('Character is not locked by us');
        }
        await this.client.sendStateEvent(characterRoomId, StateEventType.Character, '', data);
    }

    public async verifyCharacterLock(characterRoomId: string, clientId: string): Promise<boolean> {
        const event = await this.loadCharacterLock(characterRoomId);
        if (event && event.clientId === clientId && event.serverId === this.serverId) {
            return true;
        }
        return false;
    }

    public async verifyCharacterOwnership(characterRoomId: string, matrixUser: string) {
        try {
            const members = await this.client.getJoinedRoomMembers(characterRoomId);
            return members.includes(matrixUser);
        } catch (error) {
            // TODO Only catch the error if the room does not exist. Pass through network and API errors.
            console.debug(error);
            return false;
        }
    }

    public async verifyLobbyMembership(lobbyRoomId: string, matrixUser: string) {
        try {
            const members = await this.client.getJoinedRoomMembers(lobbyRoomId);
            return members.includes(matrixUser);
        } catch (error) {
            // TODO Only catch the error if the room does not exist. Pass through network and API errors.
            console.debug(error);
            return false;
        }
    }
}
