import { html, render, useCallback, useEffect, useState } from './htm/preact/standalone.module.js';

const resolveMatrixServerUrl = async(serverDomain) => {
    if (!serverDomain) return;
    const res = await fetch(`https://${serverDomain}/.well-known/matrix/client`);
    if (!res.ok) {
        return;
    }
    const data = await res.json();
    if (!data || !data['m.homeserver'] || typeof data['m.homeserver'].base_url !== 'string') {
        return;
    }
    return data['m.homeserver'].base_url;
};

function LobbyAndCharaterPicker() {
    const [busyMessage, setBusyMessage] = useState(null);
    const [client] = useState(matrixcs.createClient({
        baseUrl: localStorage.getItem('mx_hs_url'),
        accessToken: localStorage.getItem('mx_access_token'),
        userId: localStorage.getItem('mx_user_id'),
    }));
    const [characters, setCharacters] = useState([]);
    const [lobbies, setLobbies] = useState(window.appConfig.lobbies.map(lobby => ({
        joined: undefined,
        name: lobby.name,
        roomId: lobby.roomId,
    })));

    const [selectedCharacter, setSelectedCharacter] = useState();
    const [selectedLobby, setSelectedLobby] = useState();

    const [name, setName] = useState('');

    useEffect(async() => {
        setBusyMessage('Loading lobbies and characters…');
        await client.startClient({ initialSyncLimit: 0, lazyLoadMembers: true, disablePresence: true });
        client.once('sync', function (state, prevState, res) {
            if (state === 'PREPARED') {
                const characterRooms = [];
                const lobbyRooms = [];
                for (const room of client.getVisibleRooms()) {
                    if (room.selfMembership !== 'join') continue;
                    const roomType = room.currentState.getStateEvents('m.room.create', '').event.content.type;
                    if (roomType === 'de.chrpaul.city.character') {
                        characterRooms.push(room);
                    } else if (roomType === 'de.chrpaul.city.lobby') {
                        lobbyRooms.push(room);
                    }
                }
                // console.debug(characterRooms, lobbyRooms);
                setCharacters(characterRooms.map(room => ({
                    name: room.name,
                    roomId: room.roomId,
                })));
                setLobbies(lobbies => {
                    lobbies = [...lobbies];
                    for (const room of lobbyRooms) {
                        const lobbyInfo = ((room.currentState.getStateEvents('de.chrpaul.city.lobby.server', '') || {}).event || {}).content || {};
                        const props = {
                            joined: true,
                            name: room.name,
                            roomId: room.roomId,
                            url: lobbyInfo.url,
                            userId: lobbyInfo.userId,
                        };
                        const index = lobbies.findIndex(l => room.roomId === l.roomId);
                        if (index !== -1) {
                            lobbies[index] = {
                                ...lobbies[index],
                                ...props,
                            };
                        } else {
                            lobbies.push(props);
                        }
                    }
                    for (let i = 0; i < lobbies.length; i++) {
                        if (lobbies[i].joined === undefined) {
                            lobbies[i] = {
                                ...lobbies[i],
                                joined: false,
                            };
                        }
                    }
                    return lobbies;
                });
                setBusyMessage('Done');
            } else {
                console.warn(state);
            }
        });
    }, []);

    const handleJoin = useCallback(async(event) => {
        event.preventDefault();
        event.stopPropagation();
        const roomId = event.target.dataset.roomid;
        setLobbies(lobbies => {
            lobbies = [...lobbies];
            const index = lobbies.findIndex(l => l.roomId === roomId);
            if (index !== -1) {
                lobbies[index] = {
                    ...lobbies[index],
                    joined: true,
                };
            }
            return lobbies;
        });
        try {
            console.log('join', roomId);
            await client.joinRoom(roomId);
        } catch (error) {
            console.warn(error);
            setLobbies(lobbies => {
                lobbies = [...lobbies];
                const index = lobbies.findIndex(l => l.roomId === roomId);
                if (index !== -1) {
                    lobbies[index] = {
                        ...lobbies[index],
                        joined: false,
                    };
                }
                return lobbies;
            });
            setBusyMessage('Failed to join lobby.');
        }
    }, [client]);

    const handleSubmit = useCallback(async(event) => {
        event.preventDefault();
        event.stopPropagation();
        if (!event.target.elements.character || !event.target.elements.lobby) return false;
        const characterId = event.target.elements.character.value;
        // TODO Use a random, persistent clientId
        const clientId = 'abc';
        setBusyMessage('Requesting session…');
        await client.sendEvent(
            characterId,
            'de.chrpaul.city.session-request',
            {
                characterRoomId: selectedCharacter,
                lobbyRoomId: selectedLobby,
                clientId,
            },
        );
        await new Promise(resolve => setTimeout(resolve, 5000));
        const lockEvent = await client.getStateEvent(characterId, 'de.chrpaul.city.character.lock', '');
        console.debug('lockEvent', lockEvent);
        if (lockEvent && lockEvent.clientId === clientId && lockEvent.clientAccessToken) {
            setBusyMessage('Session received. Starting game…');
            localStorage.setItem('city_access_token', lockEvent.clientAccessToken);
            // TODO Get API URL from lobby room's state event
            localStorage.setItem('city_api_url', '/api');
            window.location.assign('game.html');
        } else {
            setBusyMessage('Failed to get session.');
        }
    }, [client, selectedCharacter, selectedLobby]);

    const handleSelectCharacter = useCallback(async (event) => {
        setSelectedCharacter(event.target.value);
    }, [setSelectedCharacter]);

    const handleSelectLobby = useCallback(async (event) => {
        setSelectedLobby(event.target.value);
    }, [setSelectedLobby]);

    const handleCharacterCreation = useCallback(async(event) => {
        event.preventDefault();
        event.stopPropagation();
        try {
            await client.sendEvent(selectedLobby, 'de.chrpaul.city.character-request', {
                name,
            });
            setBusyMessage('Please accept the room invite in Element, wait a minute and reload this page.');
        } catch(error) {
            setBusyMessage('Failed to send character request.');
        }
    }, [name]);

    return html`
        ${busyMessage && html`<p>${busyMessage}</p>`}
        <form onSubmit=${handleSubmit}>
            <h2>Lobbies</h2>
            <ul style="list-style:none;padding-left:0">
                ${lobbies.map(lobby => html`
                    <li key=${lobby.roomId}>
                        <label>
                            <input
                                name="lobby"
                                type="radio"
                                required
                                value=${lobby.roomId}
                                onChange=${handleSelectLobby}
                            />
                            ${lobby.name} ${lobby.joined === undefined && '(loading…)'}${lobby.joined === false && html`<button type="button" data-roomid=${lobby.roomId} onClick=${handleJoin}>join</button>`}
                        </label>
                    </li>
                `)}
            </ul>

            <h2>Characters</h2>
            <ul style="list-style:none;padding-left:0">
                ${characters.map(character => html`
                    <li key=${character.roomId}>
                        <label>
                            <input
                                name="character"
                                type="radio"
                                value=${character.roomId}
                                required
                                onChange=${handleSelectCharacter}
                            />
                            ${character.name}
                        </label>
                    </li>
                `)}
            </ul>
            ${selectedLobby && html`
                <p>
                    <input value=${name} onChange=${event => setName(event.target.value)} />
                    <button type="button" onClick=${handleCharacterCreation}>Create character</button>
                </p>
            `}

            <button disabled=${!selectedCharacter || !selectedLobby}>Play</button>
        </form>
        `;
}

function LoginForm({onLoggedIn}) {
    const [busyMessage, setBusyMessage] = useState(null);

    const handleSubmit = useCallback(async(event) => {
        event.preventDefault();
        event.stopPropagation();
        setBusyMessage('Finding homeserver…');
        const userId = event.target.elements.userId.value;
        const password = event.target.elements.password.value;
        let accessToken = event.target.elements.accessToken.value;
        const serverDomain = userId.replace(/^.*:/, '');
        const baseUrl = (await resolveMatrixServerUrl(serverDomain)) || `https://${serverDomain}`;
        setBusyMessage('Connecting to homeserver…');
        if (!accessToken) {
            const client = matrixcs.createClient({
                baseUrl,
                userId,
            });
            try {
                const response = await client.login('m.login.password', {
                    identifier: {
                        type: 'm.id.user',
                        user: userId,
                    },
                    initial_device_display_name: 'CityGame WebClient',
                    password,
                });
                accessToken = response.access_token;
            } catch {
                setBusyMessage('Failed to log in.');
                return;
            }
        }
        localStorage.setItem('mx_hs_url', baseUrl);
        localStorage.setItem('mx_user_id', userId);
        localStorage.setItem('mx_access_token', accessToken);
        onLoggedIn();
    }, [setBusyMessage, onLoggedIn]);

    return html`
        ${busyMessage && html`<p>${busyMessage}</p>`}
        <form onSubmit=${handleSubmit}>
            <p>
                <label>Matrix ID</label>
                <input name="userId" pattern="^@.+:.+$" placeholder="e.g. @alice:matrix.org" required />
            </p>

            <p>
                <label>Password</label>
                <input name="password" type="password" />
            </p>
            <p>A login creates a new session without E2EE enabled. You won't need to verify it.</p>

            <p>
                <strong>OR</strong>
            </p>

            <p>
                <label>Access token</label>
                <input name="accessToken" placeholder="e.g. syt_ABCDEFG123456…" type="password" />
            </p>

            <button>Log in</button>
        </form>
    `;
}

function GuestLogin({apiUrl}) {
    const [busyMessage, setBusyMessage] = useState(null);
    const handleLogin = useCallback(async() => {
        const response = await fetch(`${apiUrl}/guest-session`);
        if (!response.ok) {
            setBusyMessage('Failed to log in as a guest.');
            return;
        }
        const { accessToken } = await response.json();
        setBusyMessage('Session received. Starting game…');
        localStorage.setItem('city_access_token', accessToken);
        localStorage.setItem('city_api_url', apiUrl);
        window.location.assign('game.html');
    });

    return html`
        ${busyMessage && html`<p>${busyMessage}</p>`}
        <p>
            <button type="button" onClick=${handleLogin}>Play as guest</button>
        </p>
    `;
}

function App() {
    const [loggedIn, setLoggedIn] = useState(!!localStorage.getItem('mx_hs_url'));

    const handleLogout = useCallback((event) => {
        event.preventDefault();
        event.stopPropagation();
        const client = matrixcs.createClient({
            baseUrl: localStorage.getItem('mx_hs_url'),
            accessToken: localStorage.getItem('mx_access_token'),
            userId: localStorage.getItem('mx_user_id'),
        });
        client.logout().catch(error => {
            console.warn('Failed to log out old session.', error);
        });
        localStorage.removeItem('mx_user_id');
        localStorage.removeItem('mx_hs_url');
        localStorage.removeItem('mx_access_token');
        setLoggedIn(false);
    }, [setLoggedIn]);

    const handleLoggedIn = useCallback(() => {
        setLoggedIn(true);
    }, [setLoggedIn]);

    return html`
        ${loggedIn ?
            html`
                <p><button type="button" onClick=${handleLogout}>Log out</button></p>
                <${LobbyAndCharaterPicker} />
            `
        :
            html`
                ${window.appConfig.guestApi && html`<h2>Play without an account</h2><${GuestLogin} apiUrl=${window.appConfig.guestApi} /><hr /><br />`}
                <h2>Log in with a Matrix account</h2>
                <p><strong>While I use my regular Matrix account, you may want to use a separate Matrix account for experiments and gaming like this. Any client is a risk, especially unstable ones like this.</strong></p>
                <${LoginForm} onLoggedIn=${handleLoggedIn} />
                <hr />
                <p><a href="https://gitlab.com/jaller94/hacker-town">Repository on Gitlab.com</a></p>
            `
        }
    `;
}

async function fetchConfig() {
    const response = await fetch('./config.json');
    return await response.json();
}

async function main() {
    if (!window.localStorage) {
        html`<p>Your browser must support localStorage. This page may not work with Incognito Mode.</p>`;
    }

    window.appConfig = await fetchConfig();
    const view = document.getElementById('app');
    render(html`<${App} />`, view);
}

main();
