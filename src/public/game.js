import { html, render, useCallback, useEffect, useState } from './htm/preact/standalone.module.js';

const accessToken = localStorage.getItem('city_access_token');
const apiUrl = localStorage.getItem('city_api_url');
if (!accessToken || !apiUrl) {
    window.location.assign('./');
}

const eventBroker = document.createElement('div');

const socket = new WebSocket('ws://localhost:3000/api', 'hacker-town-2022-01-23');
socket.addEventListener('error', (event) => {
    console.log('error', event);
});
socket.addEventListener('message', (event) => {
    const data = JSON.parse(event.data);
    console.log('message', data);
    if (data.map) {
        const event = new CustomEvent('map', {
            bubbles: true,
            detail: data.map,
        });
        console.debug('map on message', data.map);
        eventBroker.dispatchEvent(event);
    }
    if (data.tile) {
        const event = new CustomEvent('tile', {
            bubbles: true,
            detail: data.tile,
        });
        console.debug('tile on message', data.tile);
        eventBroker.dispatchEvent(event);
    }
    if (data.player && typeof data.player.money === 'number') {
        const event = new CustomEvent('money', {
            bubbles: true,
            detail: data.player.money,
        });
        console.debug('money on message', data.player.money);
        eventBroker.dispatchEvent(event);
    }
    if (data.player && typeof data.player.displayName === 'string') {
        const event = new CustomEvent('playerName', {
            bubbles: true,
            detail: data.player.displayName,
        });
        console.debug('playerName on message', data.player.displayName);
        eventBroker.dispatchEvent(event);
    }
    if (data.player && typeof data.player.x === 'number' && typeof data.player.y === 'number') {
        const event = new CustomEvent('position', {
            bubbles: true,
            detail: {
                x: data.player.x,
                y: data.player.y,
            },
        });
        console.debug('position on message', data.player);
        eventBroker.dispatchEvent(event);
    }
    if (data.global && data.global.onlinePlayerCount) {
        const event = new CustomEvent('playersOnline', {
            bubbles: true,
            detail: data.global.onlinePlayerCount,
        });
        console.debug('playersOnline on message', data.global.onlinePlayerCount);
        eventBroker.dispatchEvent(event);
    }
});
socket.addEventListener('open', (event) => {
    setTimeout(() => {
        socket.send(JSON.stringify({accessToken}));
    }, 500);
});

function Tile({alt, img}) {
    if (!img) {
        return html`<div class="tile" />`;
    }
    return html`<img class="tile" src=${apiUrl + '/images/' + img + '.jpg'} alt=${alt} />`;
}

const classnames = (obj) => {
    return Object.entries(obj).filter(([, value]) => value).map(([key]) => key).join(' ');
};

const XOR = (a, b) => {
    return (a || b) && !(a && b);
}

function Map({center, tiles, onTileClick}) {
    const handleTileClick = useCallback((event) => {
        const data = event.target.attributes;
        const x = Number.parseInt(data['data-x'].value);
        const y = Number.parseInt(data['data-y'].value);
        if (Number.isNaN(x) || Number.isNaN(y)) {
            console.error('Cannot change tile', x, y);
            return;
        }
        onTileClick(x, y);
    });
    return html`
        <table>
            ${tiles.map(row => (html`
                <tr key=${row[0].y}>
                    ${row.map(cell => {
                        let title = '';
                        if (cell.name) {
                            title = `${cell.name} `;
                        }
                        title = `(${cell.x}|${cell.y})`;
                        const walkable = cell.walkable && XOR(Math.abs(center[0] - cell.x) === 1, Math.abs(center[1] - cell.y) === 1);
                        const classNames = classnames({
                            center: center[0] === cell.x && center[1] === cell.y,
                            walkable: walkable,
                        });
                        return html`
                            <td key=${cell.x} class=${classNames} data-x=${cell.x} data-y=${cell.y} onClick=${walkable && handleTileClick} title=${title}>
                                <${Tile} img=${cell.img} alt=${title} />
                            </td>
                        `;
                    })}
                </tr>
            `))}
        </table>
    `;
}


function getView(map, x, y, width = 1, height = 1) {
    const view = [];
    for (let yOffset = 0; yOffset < height; yOffset++) {
        const row = [];
        view.push(row);
        for (let xOffset = 0; xOffset < width; xOffset++) {
            const tile = {
                ...(map[y+yOffset] ?? [])[x+xOffset] ?? {},
                x: x + xOffset,
                y: y + yOffset,
            };
            row.push(tile);
        }
    }
    return view;
}

function getViewByRadius(map, x, y, radius = 2) {
    return getView(map, x - radius, y - radius, 2 * radius + 1, 2 * radius + 1);
}


const step = ([x, y], [x2, y2]) => {
    if (x2 < x) {
        return [x - 1, y];
    }
    if (x2 > x) {
        return [x + 1, y];
    }
    if (y2 < y) {
        return [x, y - 1];
    }
    if (y2 > y) {
        return [x, y + 1];
    }
    return null;
};

function Colldown({ on }) {
    return html`
        <div class="cooldown ${on ? 'active' : ''}">Busy walking…</div>
    `;
}

function PlayerCount({count}) {
    if (count === undefined) return null;
    if (count === 1) return html`You are the only one online.`;
    return html`There are ${count} players online.`;
}

function LogOutButton() {
    const handleClick = useCallback(async() => {
        const response = await fetch(`${apiUrl}/end-session`, {
            method: 'POST',
            headers: {
                'Authorization': `Bearer ${accessToken}`,
            },
        });
        if (!response.ok) {
            console.warn('Failed to end session', response);
        }
        window.location.assign('./');
    });

    return html`<button type="button" onClick=${handleClick}>End session</button>`;
}

function Game() {
    const [map, setMap] = useState(null);
    const [tile, setTile] = useState(null);
    const [moveCooldown, setMoveCooldown] = useState(null);
    const [position, setPosition] = useState([0, 0]);
    const [targetPosition, setTargetPosition] = useState(null);
    const [playerCount, setPlayerCount] = useState(null);
    const [playerName, setPlayerName] = useState(null);
    const [money, setMoney] = useState(0);

    useEffect(async() => {
        const handleMapUpdate = (event) => {
            setMap(event.detail);
        };

        const handleTileUpdate = (event) => {
            setTile(event.detail);
        };

        const handleMoneyUpdate = (event) => {
            setMoney(event.detail);
        };

        const handlePlayerNameUpdate = (event) => {
            setPlayerName(event.detail);
        };

        const handlePlayersOnlineUpdate = (event) => {
            setPlayerCount(event.detail);
        };

        const handlePositionUpdate = (event) => {
            setPosition([event.detail.x, event.detail.y]);
        };

        eventBroker.addEventListener('map', handleMapUpdate);
        eventBroker.addEventListener('tile', handleTileUpdate);
        eventBroker.addEventListener('money', handleMoneyUpdate);
        eventBroker.addEventListener('playerName', handlePlayerNameUpdate);
        eventBroker.addEventListener('playersOnline', handlePlayersOnlineUpdate);
        eventBroker.addEventListener('position', handlePositionUpdate);

        return () => {
            eventBroker.removeEventListener('map', handleMapUpdate);
            eventBroker.removeEventListener('tile', handleTileUpdate);
            eventBroker.removeEventListener('money', handleMoneyUpdate);
            eventBroker.removeEventListener('playerName', handlePlayerNameUpdate);
            eventBroker.removeEventListener('playersOnline', handlePlayersOnlineUpdate);
            eventBroker.removeEventListener('position', handlePositionUpdate);
        };
    }, []);

    useEffect(async() => {
        if (!targetPosition) {
            return;
        }
        if (position[0] === targetPosition[0] && position[1] === targetPosition[1]) {
            setTargetPosition(null);
        }
        if (moveCooldown) {
            return;
        }
        const nextStep = step(position, targetPosition);
        if (nextStep) {
            console.log("nextStep");
            // setPosition(nextStep);
            socket.send(JSON.stringify({
                type: 'move',
                content: {
                    x: nextStep[0],
                    y: nextStep[1],
                },
            }));
            setMoveCooldown(setTimeout(() => {
                setMoveCooldown(null);
            }, 300));
        }
    }, [moveCooldown, position, targetPosition]);

    const handleAction = useCallback(async(event) => {
        const action = event.target.dataset.action;
        socket.send(JSON.stringify({
            type: 'action',
            content: {
                action,
            },
        }));
    }, []);

    const handleTileClick = useCallback((x, y) => {
        if (!getView(map, x, y)[0][0].walkable) {
            return;
        }
        setTargetPosition([x,y]);
    }, [map]);

    if (!map) return null;
    return html`
        <h2></h2>
        <p>Click on a neighbouring tile to walk there. Every step takes some time.</p>
        <p>Your name is ${playerName}. <${PlayerCount} count=${playerCount} /> <${LogOutButton} /></p>
        <p>Money: $${money}</p>
        <div class="app">
            <div>
                <${Map} center=${position} tiles=${getViewByRadius(map, position[0], position[1])} onTileClick=${handleTileClick} />
                <${Colldown} on=${!!moveCooldown} />
            </div>
            
            <div>
                ${tile && !!tile.name && html`
                    <h2>${tile.name}</h2>
                    ${(tile.description ?? '').split('\n').map(text => html`<p>${text}</p>`)}
                `}
                ${tile && !!tile.actions && html`
                    <h3>Actions</h3>
                    <ul>
                        ${tile.actions.map(action => html`
                            <li key=${action.name}>
                                <button type="button" data-action=${action.name} onClick=${handleAction}>${action.name}</button>
                            </li>
                        `)}
                    </ul>
                `}
                ${tile && !!tile.players && html`
                    <h3>Players</h3>
                    <ul>
                        ${tile.players.map(player => html`<li>${player.displayName}</li>`)}
                    </ul>
                `}
            </div>
        </div>
    `;
}

async function main() {
    const view = document.getElementById('app');
    render(html`<${Game} />`, view);
}

main();
