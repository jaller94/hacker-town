export default {
    tiles: [
        [
            ,
            {
                img: 'view-of-station-platforms-in-civitavecchia-italy',
                name: 'S-Bahnstation Technikviertel B',
                description: 'Die Hummel-Station liegt auf der Linie 2 und verbindet die westlichen Außenbezirke mit den östlichen.',
                walkable: true,
                teleports: [
                    {
                        target: [1, 0],
                    },
                ],
            },
            {
                img: 'woodruff-park-1516827',
                name: 'Technikviertel B',
                description: 'Alle großen Marken sind im Technikviertel B nahe des Musik-Parks anwesend. Hier können zu jedem Standard-Gerät Ersatzteile und Wartungsverträge bestellt werden.',
                walkable: true,
            },
        ],
        [
            ,
            ,
            {
                img: 'centinnial-park-georgia-1569554',
                name: 'Nordosteingang (Musik-Park)',
                description: 'Eine große Liegewiese, welche vor allem bei Hunden bliebt ist, grenzt den Musik-Park von der nördlichen Büro-Gegend ab.',
                walkable: true,
            },
            ,
            ,
            {
                img: 'city-1570944',
                name: 'S-Bahnstation Stadtzentrum',
                description: 'Die Hummel-Station liegt auf der Linie 2 und verbindet die westlichen Außenbezirke mit den östlichen.',
                walkable: true,
                teleports: [
                    {
                        target: [1,0],
                    },
                ],
            },
        ],
        [
            {
                img: 'park-1469101',
                name: 'Salsa-Promenade (Musik-Park)',
                description: 'Der perfekte Ort zum Ausruhen in der Sonne oder auch zum schwungvollen Tanz findet sich hier. Die Salsa-Promenade spendet etwas Schatten, aber erlaubt dennoch den Sonnenschein zu genießen.',
                walkable: true,
            },
            {
                img: 'park-in-ny-1461016',
                name: 'Beethoven-Brücke (Musik-Park)',
                description: '',
                walkable: true,
            },
            {
                img: 'city-1467157',
                name: 'Fontäne der Künste (Musik-Park)',
                description: 'Im Musik-Park befindet sich ein kleiner See mit einer großen Fontäne. An heißen Tagen wie diesem, dient er spielenden Kindern und Hunden zur Abkühlung.\nUm den Brunnen befinden sich Bänke mit ein paar Personen. Einige sehen so aus als könnten sie Waren günstig unter der Hand verkaufen.',
                walkable: true,
            },
            {
                img: 'central-park-1566597',
                name: 'Osteingang (Musik-Park)',
                description: 'Hier am Rand des Musik-Parks treffen Stadtnatur und Innenstadt aufeinander. Unter den Baumkronen sind in der Ferne die Wolkenkrazer auf fast allen Seiten zu sehen.',
                walkable: true,
            },
            {
                img: 'city-1-1218552',
                name: 'Hasebrücke',
                description: 'Die Hasebrücke, benannt nach ihrer Architektin Franka Hase, ist die Verbindung zwischen Westend und der Innenstadt.',
                walkable: true,
            },
            {
                img: 'buildings-in-the-city-13-1507215',
                name: 'Überdachter Fußgängerweg',
                description: 'Die Überdachung der Bürogebäude macht diesen Teil der Straße auch im Regen angenehm. Ein Glück, dass heute die Sonne scheint.',
                walkable: true,
            },
            {
                img: 'nyc-1218737',
                name: 'Büroviertel A',
                description: 'Im hektischen Büroviertel A mischen sich unter die Masse der großen Firmen einige Start Ups. Die Konkurrenz ist hart und die hohen Erwartungen der Investoren erlauben kaum Fehltritte.\nVor deiner Nase stolpert ein Anzugträger über den Dackel einer fein gekleideten Dame. Sie schnauzt ihn an während ihr Hund verschreckt hinter sie weicht. Die Hektik bietet sich an für einen Taschendiebstahl.',
                walkable: true,
                actions: [
                    {
                        name: 'Taschendiebstahl',
                    },
                ],
            },
            {
                img: 'manhattan-skyline-new-york-city-august-2007-3-1215804',
                name: 'Stadtmitte',
                description: 'Im Herzen der Stadt kann man in allen vier Himmelsrichtung Wege sehen.',
                walkable: true,
            },
            {
                img: 'from-above-1233568',
                name: 'Tourismus-Viertel',
                description: 'Wer es sich leisten kann, wird ein Hotelapartment direkt im Stadtzentrum buchen. Die Menschen auf den Straßen sind zumeist hervorragend gekleidete Geschäftspersonen.',
                walkable: true,
            }
        ],
        [
            {
                img: 'park-1360686',
                name: 'Kunsthecken (Musik-Park)',
                description: '',
                walkable: true,
            },
            {
                img: 'bandshell-theater-in-sioux-city-iowa',
                name: 'Freilufttheater (Musik-Park)',
                description: '',
                walkable: true,
            },
            {
                img: 'park-path-1549761',
                name: 'Walzer-Promenade (Musik-Park)',
                description: 'Eine Rad- und Fußgängerweg führt entlang von Bäumen durch den Park.',
                walkable: true,
            }
            ,
            ,
            ,
            ,
            ,
            {
                img: 'sydney-skyline-1220202',
                name: 'Technikviertel A',
                description: 'Im hektischen Büroviertel A mischen sich unter die Masse der großen Firmen einige Start Ups. Die Konkurrenz ist hart und die hohen Erwartungen der Investoren erlauben kaum Fehltritte.\nVor deiner Nase stolpert ein Anzugträge über den Dackel einer fein gekleideten Dame. Sie schnauzt ihn an während ihr Hund verschreckt hinter sie weicht. Die Hektik bietet sich an für einen Taschendiebstahl.',
                walkable: true,
            },
        ],
        [
            ,
            ,
            {
                img: 'national-library-1223589',
                name: 'Bücherei: Hauptzweig',
                description: 'Eindrucksvoll ersteckt sich der mehrstöckige Hauptsitz der Bücherei in den Himmel. Jede Etage ist voller Regale mit jeweils tausenden Büchern. Eine schier unermessliche Ansammlung von Wissen, welches allen Besuchern zur Ansicht bereit steht.\nDes Weiteren befinden sich in der Bücherei öffentliche Computer, ein kostenloses WLAN und ein Aufnahmestudio für Audio- und Video-Podcasts.',
                walkable: true,
            },
            ,
            ,
            ,
            ,
            {
                img: 'city-hall-and-police-department-building-in-kokomo-indiana',
                name: 'Polizeistation: Hauptzweig',
                description: 'Der Hauptzweig der Polizei ist ein gut bewachtes Gebäude auf dessen Parkplatz sich viele Einsatzfahrzeuge befinden. Ab und zu verlassen oder betreten Polizist:innen das Gebäude.\nDer Eingang steht jeder Person offen, die ein Verbrechen melden möchte oder Schutz sucht.',
                walkable: true,
            },
        ],
    ],
};
