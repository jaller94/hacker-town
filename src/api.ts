import express from 'express';
import path from 'path';
import { GameMap } from './index';
import SessionsController from './sessions';

export type Player = {
    displayName: string;
    money: number,
    x: number;
    y: number;
    lastActivity: number;
};

const doesTileHaveAction = (map: GameMap, position: {x: number, y: number}, action: string): boolean => {
    const actions = map.tiles[position.y][position.x]?.actions;
    if (Array.isArray(actions)) {
        return actions.findIndex(a => a.name === action) !== -1;
    }
    return false;
};

const routerGenerator = (map: GameMap, sessions: SessionsController) => {
    const router = express.Router();

    const sessionLoader = (req: express.Request, res: express.Response, next: any) => {
        if (!req.headers.authorization) {
            return res.status(403).json({
                error: 'INVALID_SESSION',
            });
        }
        const token = req.headers.authorization.replace('Bearer ', '');
        const session = sessions.getSession(token);
        if (!session) {
            return res.status(403).json({
                error: 'INVALID_SESSION',
            });
        }
        req.gameSession = session;
        next();
    };
    
    router.get('/', sessionLoader, async (req, res) => {
        let player = req.gameSession.liveData as Player;
        const {x, y} = player;
        res.json({
            global: {
                onlinePlayerCount: sessions.size,
            },
            map: map.tiles,
            player,
            tile: {
                ...map.tiles[y][x],
                players: sessions.players.filter(p => p.x === x && p.y === y && p !== player),
            },
        });
    });

    router.post('/end-session', async (req, res) => {
        if (!req.headers.authorization) {
            return res.status(403).json({
                error: 'INVALID_SESSION',
            });
        }
        const token = req.headers.authorization.replace('Bearer ', '');
        sessions.closeSession(token);
        console.log(`ended session ${token}`);
        res.json({});
    });

    router.get('/guest-session', async (req, res) => {
        const accessToken = await sessions.addSession();
        res.json({
            accessToken,
        });
    });

    router.use(express.json({
        strict: true,
    }));

    router.post('/move', sessionLoader, async (req, res) => {
        let player = req.gameSession.liveData as Player;
        const x = Number.parseInt(req.body.x);
        const y = Number.parseInt(req.body.y);
        if (Number.isNaN(x) || Number.isNaN(y) || x < 0 || y < 0) {
            return res.status(400).json({ error: 'INVALID_TARGET_LOCATION' });
        }
        if (!map.tiles[y][x]?.walkable) {
            return res.status(400).json({ error: 'INVALID_TARGET_LOCATION' });
        }
        if (Math.abs(player.x! - x) + Math.abs(player.y! - y) > 1) {
            return res.status(403).json({ error: 'INVALID_TARGET_LOCATION', subError: 'Trying to move more than one field.' });
        }
        player.x = x;
        player.y = y;
        player.lastActivity = Date.now();
        res.json({
            global: {
                onlinePlayerCount: sessions.size,
            },
            player: {
                x,
                y,
            },
            tile: {
                ...map.tiles[y][x],
                players: sessions.players.filter(p => p.x === x && p.y === y && p !== player),
            },
        });
    });

    router.post('/action/:action', sessionLoader, async (req, res) => {
        let player = req.gameSession.liveData as Player;
        const action = req.params.action;
        if (action === 'Taschendiebstahl' && doesTileHaveAction(map, player, 'Taschendiebstahl')) {
            player.money += Math.round(Math.random() * 4 + Math.random() * 4);
            return res.json({
                global: {
                    onlinePlayerCount: sessions.size,
                },
                player: {
                    money: player.money,
                },
                tile: {
                    players: sessions.players.filter(p => p.x === player.x && p.y === player.y && p !== player),
                },
            });
        }
        return res.status(400).json({ error: 'INVALID_ACTION' });
    });

    router.use('/images', express.static(path.join(__dirname, '../data/images')));

    const getMetrics = async () => {
        return `# HELP active_players Number of buckets that currently exist.
    # TYPE active_players gauge
    active_players ${sessions.size}`;
    };

    return router;
}

export default routerGenerator;
