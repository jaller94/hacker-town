import fs from 'fs';
import path from 'path';
import ws from 'ws';
import App from './app';
import WebsocketApi from './websocket-api';
import log from './logger';
import MatrixController from './matrix';
import SessionsController from './sessions';

export type Tile = {
    actions?: {
        name: string;
    }[];
    description: string;
    img?: string;
    name: string;
    walkable: boolean;
    playerCount?: number;
};

export type Tiles = (Tile | undefined)[][];

export type GameMap = {
    tiles: Tiles;
};

async function main() {
    const port = process.env.PORT || 3000;

    if (!process.env.MATRIX_BASE_URL || !process.env.MATRIX_ACCESS_TOKEN || !process.env.MATRIX_LOBBY_ROOM) {
        throw Error('Matrix ENV vars not found');
    }
    const matrixController = new MatrixController(
        process.env.MATRIX_BASE_URL,
        process.env.MATRIX_ACCESS_TOKEN,
        process.env.MATRIX_LOBBY_ROOM,
    );
    const sessionsController = new SessionsController(matrixController);

    const map: GameMap = JSON.parse(fs.readFileSync(path.join(__dirname, '../data/maps/fosdem.json'), 'utf8'));
    
    const app = App(map, matrixController, sessionsController);
    const server = app.listen(port);

    // Set up a headless websocket server that prints any
    // events that come in.
    const wsServer = new ws.Server({ noServer: true });
    WebsocketApi(wsServer, map, matrixController, sessionsController);

    // `server` is a vanilla Node.js HTTP server, so use
    // the same ws upgrade process described here:
    // https://www.npmjs.com/package/ws#multiple-servers-sharing-a-single-https-server
    server.on('upgrade', (request, socket, head) => {
        wsServer.handleUpgrade(request, socket, head, socket => {
            wsServer.emit('connection', socket, request);
        });
    });

    log.info(`Listing on port ${port}`);
}

main().catch(log.error);
