import ws from 'ws';
import { GameMap, Tile, Tiles } from './index';
import MatrixController from './matrix';
import SessionsController, { Session } from './sessions';

const doesTileHaveAction = (map: GameMap, position: {x: number, y: number}, action: string): boolean => {
    const actions = map.tiles[position.y][position.x]?.actions;
    if (Array.isArray(actions)) {
        return actions.findIndex(a => a.name === action) !== -1;
    }
    return false;
};

const getMap = (map: GameMap): Tiles => {
    const tiles: Tiles = [];
    for (let y = 0; y < 30; y++) {
        const row: (Tile | undefined)[] = [];
        for (let x = 0; x < 30; x++) {
            let tile;
            try {
                tile = map.tiles[y][x];
                if (tile) {
                    tile = {
                        ...tile,
                        img: tile?.img ?? 'walkable',
                    };
                }
            } catch { }
            row.push(tile ?? undefined);
        }
        tiles.push(row);
    }
    return tiles;
};

// const getMapView = (map: GameMap, position: {x: number, y: number} ): Tiles => {
//     const range = 2;
//     const tiles: Tiles = [];
//     for (let y = position.y - range; y < position.y + range; y++) {
//         const row: (Tile | undefined)[] = [];
//         for (let x = position.x - range; x < position.x + range; x++) {
//             let tile;
//             try {
//                 tile = map.tiles[y][x];
//                 if (tile) {
//                     tile = {
//                         ...tile,
//                         img: tile?.img ?? 'walkable',
//                     };
//                 }
//             } catch {}
//             row.push(tile ?? undefined);
//         }
//         tiles.push(row);
//     }
//     return tiles;
// };

const routerGenerator = (wsServer: ws.Server, map: GameMap, matrixController: MatrixController, sessions: SessionsController) => {
    wsServer.on('connection', socket => {
        let session: Session | undefined;
        socket.on('message', message => {
            let data;
            try {
                data = JSON.parse(message.toString());
            } catch {
                socket.close();
                return;
            }
            if (typeof data.accessToken === 'string') {
                session = sessions.getSession(data.accessToken);
                if (session) {
                    const player = session.liveData;
                    const { x, y } = player;
                    socket.send(JSON.stringify({
                        global: {
                            onlinePlayerCount: sessions.size,
                        },
                        map: getMap(map),
                        player,
                        tile: {
                            ...map.tiles[y][x],
                            players: sessions.players.filter(p => p.x === x && p.y === y && p !== player),
                        },
                    }));
                }
            }
            if (data.type === 'action') {
                if (session) {
                    const player = session.liveData;
                    const action = data.content.action;
                    console.log('action', action, player, doesTileHaveAction(map, player, action));
                    if (action === 'Taschendiebstahl' && doesTileHaveAction(map, player, 'Taschendiebstahl')) {
                        player.money += Math.round(Math.random() * 4 + Math.random() * 4);
                        socket.send(JSON.stringify({
                            player: {
                                money: player.money,
                            },
                        }));
                    }

                }
            } else if (data.type === 'move') {
                if (session) {
                    const {x, y} = data.content;
                    const player = session.liveData;
                    if (Number.isNaN(x) || Number.isNaN(y) || x < 0 || y < 0) {
                        return { error: 'INVALID_TARGET_LOCATION' };
                    }
                    if (!session.godMode) {
                        if (!map.tiles[y][x]?.walkable) {
                            return { error: 'INVALID_TARGET_LOCATION' };
                        }
                        if (Math.abs(player.x! - x) + Math.abs(player.y! - y) > 1) {
                            return { error: 'INVALID_TARGET_LOCATION', subError: 'Trying to move more than one field.' };
                        }
                    }
                    player.x = x;
                    player.y = y;
                    player.lastActivity = Date.now();
                    socket.send(JSON.stringify({
                        player: {
                            x,
                            y,
                        },
                        tile: {
                            ...map.tiles[y][x],
                            players: sessions.players.filter(p => p.x === x && p.y === y && p !== player),
                        },
                    }));
                }
            }
            if (!session) {
                return;
            }
            console.log('new websocket message', data);
        });
    });
};

export default routerGenerator;
