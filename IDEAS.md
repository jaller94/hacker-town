# Ideas

## What should make this MUDs fun?
* **Exploration**
  * A map to discover
  * Fun and clever writing
  * Unique quests

## What may be needed?
* A map editor to quickly create new content.
* Seasonal content
  * Creating rarety
  * Deprecating old content to allow for new content.
