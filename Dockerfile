FROM node:18-alpine AS BUILD

WORKDIR "/app"

# Install app dependencies
COPY . /app/
RUN npm install
RUN npm run build
RUN npm prune --omit=dev

FROM node:18-alpine

ENV NODE_ENV production
ENV PORT 3000

# RUN mkdir -p /var/hacker-town/data
RUN mkdir -p /app/webstorage

# COPY data /var/hacker-town/data

WORKDIR "/app"

# Bundle app source
COPY --from=BUILD /app/node_modules/ node_modules/
COPY --from=BUILD /app/dist/ ./dist
COPY --from=BUILD /app/data/ ./data

EXPOSE 3000
CMD ["node", "dist/index"]
