# Hacker Town

A tile-based Massive Multiplayer Role Play Game. Players log in with their Matrix.org accounts and own their character data.

## Start
| ENV variable | Example value |
| :--- | :--- |
| MONGODB_URL | mongodb://root:example@localhost:27017/?tls=false |
| MATRIX_BASE_URL | https://matrix-client.matrix.org |
| MATRIX_ACCESS_TOKEN | syt_c2… |
| MATRIX_LOBBY_ROOM | !xu…:matrix.org |
| BASE_URL | http://localhost:3000 |

```sh
npm install
npm run start
```
